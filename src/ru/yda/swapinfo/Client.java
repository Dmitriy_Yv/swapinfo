package ru.yda.swapinfo;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             DataInputStream input = new DataInputStream(socket.getInputStream());
             DataOutputStream output = new DataOutputStream(socket.getOutputStream())) {

            while (true) {

                System.out.println("Введите слово ");
                String response = scanner.nextLine();
                output.writeUTF(response);
                output.flush();
                System.out.println("отправлено серверу: " + response);
                if (response.equals("and")) {
                    System.out.println("");
                    break;
                }
                response = input.readUTF();
                System.out.println("прислал сервер: " + response);
                Thread.sleep(2000);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}